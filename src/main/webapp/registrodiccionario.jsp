<%-- 
    Document   : registrodiccionario
    Created on : May 8, 2021, 3:17:27 PM
    Author     : const
--%>

<%@page import="java.util.Iterator"%>
<%@page import="com.mycompany.diccionarioespanoloxford.entity.Registrodepalabras"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<Registrodepalabras> palabras= (List<Registrodepalabras>)request.getAttribute("listadePalabras");
    Iterator<Registrodepalabras> itpalabras = palabras.iterator();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registro</title>
    </head>
    <body>
        <h1>Historial de Palabras Buscadas</h1>
        <form  name="form" action="ControllerOxford" method="POST">
        <table border="1">
                    <thead>
                    <th>Palabra</th>
                    <th>Definición</th> 
                    <th>Fecha</th>                 
                    <th></th>
         
                    </thead>
                    <tbody>
                        <%while (itpalabras.hasNext()) {
                       Registrodepalabras pa = itpalabras.next();%>
                        <tr>
                            <td id="palabra"><%= pa.getPalabra()%></td>
                            <td id="definicion"><%= pa.getFecha()%></td> 
                            <td id="fecha"><%= pa.getDefinicion()%></td> 
                        </tr>
                        <%}%>                
                    </tbody>           
                </table>

        </form>
        
    </body>
</html>