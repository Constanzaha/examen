<%-- 
    Document   : definicion
    Created on : May 8, 2021, 3:16:59 PM
    Author     : const
--%>

<%@page import="com.mycompany.diccionarioespanoloxford.entity.Registrodepalabras"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    Registrodepalabras definicion = (Registrodepalabras) request.getAttribute("definicion");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Definicion</title>
    </head>
    <body>
        <h1>Diccionario</h1>

        <form  name="form" action="ControllerOxford" method="POST">
            <h3><%= definicion.getPalabra().toString()%></h3>
            <p><%= definicion.getDefinicion().toString()%></p> 
            <br>
            <a href ="index.jsp">Volver a página principal</a>
        </form>




    </body>
</html>
