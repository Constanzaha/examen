<%-- 
    Document   : index
    Created on : May 8, 2021, 3:16:46 PM
    Author     : const
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Index</title>
    </head>
    <body>
        <h1>Diccionario</h1>

        <form  name="form" action="ControllerOxford" method="POST">
            <h2>Escriba la palabra cuyo significado desea saber</h2>
            <br>
            <input type="text" name="palabra" id="palabra">
            <button type="submit" name="accion" value="buscar" class="btn btn-success">Buscar</button><br/> 
            <h2>Revisar historial de palabras buscadas</h2>
            <button type="submit" name="accion" value="ver" class="btn btn-success">Historial</button><br/> 

        </form>
        
    </body>
</html>
