/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.diccionarioespanoloxford;

import cl.DTO.PalabraDTO;
import com.mycompany.diccionarioespanoloxford.dao.RegistrodepalabrasJpaController;
import com.mycompany.diccionarioespanoloxford.entity.Registrodepalabras;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author const
 */



public class ApiOxford { 
    @GET
    @Path("/{idbuscar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response significado(@PathParam("idbuscar") String idbuscar){
    
        try {
            Client client = ClientBuilder.newClient();
            
            WebTarget myResource = client.target("https://od-api.oxforddictionaries.com/api/v2/entries/es/"+idbuscar);

            PalabraDTO palabradto = myResource.request(MediaType.APPLICATION_JSON).header("api-key","3d2d3e7e35a5c32474ab2013839183e4").header("api-id","e32e3a0b").get(PalabraDTO.class );
            
            Registrodepalabras pal = new Registrodepalabras ();
            
            pal.setPalabra(palabradto.getWord());
            Date fecha = new Date();
            pal.setFecha(fecha.toString());
            
            String definicion = (String) palabradto.getResults().get(0).getLexicalEntries().get(0).getEntries().get(0).getSenses().get(0).getDefinitions().toString();
            pal.setDefinicion(definicion);
            
            RegistrodepalabrasJpaController dao = new RegistrodepalabrasJpaController();
            dao.create(pal);
            
            return Response.ok(200).entity(pal).build();
        } catch (Exception ex) {
            Logger.getLogger(ApiOxford.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
        
    }
    
   @GET
    @Path("/registro")
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarClientes(){
        
     RegistrodepalabrasJpaController dao = new  RegistrodepalabrasJpaController();     
     List<Registrodepalabras> lista = dao.findRegistrodepalabrasEntities();   
     return Response.ok(200).entity(lista).build();
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response add(Registrodepalabras registrodepalabras) {

        try {
            RegistrodepalabrasJpaController dao = new RegistrodepalabrasJpaController();
            dao.create(registrodepalabras);
        } catch (Exception ex) {
            Logger.getLogger(ApiOxford.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.ok(200).entity(registrodepalabras).build();

    }
    
    
    
}