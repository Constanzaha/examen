/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.diccionarioespanoloxford.dao;

import com.mycompany.diccionarioespanoloxford.dao.exceptions.NonexistentEntityException;
import com.mycompany.diccionarioespanoloxford.dao.exceptions.PreexistingEntityException;
import com.mycompany.diccionarioespanoloxford.entity.Registrodepalabras;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author const
 */
public class RegistrodepalabrasJpaController implements Serializable {

    public RegistrodepalabrasJpaController() {

    }
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("diccionario");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Registrodepalabras registrodepalabras) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(registrodepalabras);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findRegistrodepalabras(registrodepalabras.getPalabra()) != null) {
                throw new PreexistingEntityException("Registrodepalabras " + registrodepalabras + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Registrodepalabras registrodepalabras) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            registrodepalabras = em.merge(registrodepalabras);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = registrodepalabras.getPalabra();
                if (findRegistrodepalabras(id) == null) {
                    throw new NonexistentEntityException("The registrodepalabras with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Registrodepalabras registrodepalabras;
            try {
                registrodepalabras = em.getReference(Registrodepalabras.class, id);
                registrodepalabras.getPalabra();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The registrodepalabras with id " + id + " no longer exists.", enfe);
            }
            em.remove(registrodepalabras);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Registrodepalabras> findRegistrodepalabrasEntities() {
        return findRegistrodepalabrasEntities(true, -1, -1);
    }

    public List<Registrodepalabras> findRegistrodepalabrasEntities(int maxResults, int firstResult) {
        return findRegistrodepalabrasEntities(false, maxResults, firstResult);
    }

    private List<Registrodepalabras> findRegistrodepalabrasEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Registrodepalabras.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Registrodepalabras findRegistrodepalabras(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Registrodepalabras.class, id);
        } finally {
            em.close();
        }
    }

    public int getRegistrodepalabrasCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Registrodepalabras> rt = cq.from(Registrodepalabras.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
