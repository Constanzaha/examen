/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.diccionarioespanoloxford.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author const
 */
@Entity
@Table(name = "registrodepalabras")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Registrodepalabras.findAll", query = "SELECT r FROM Registrodepalabras r"),
    @NamedQuery(name = "Registrodepalabras.findByPalabra", query = "SELECT r FROM Registrodepalabras r WHERE r.palabra = :palabra"),
    @NamedQuery(name = "Registrodepalabras.findByDefinicion", query = "SELECT r FROM Registrodepalabras r WHERE r.definicion = :definicion"),
    @NamedQuery(name = "Registrodepalabras.findByFecha", query = "SELECT r FROM Registrodepalabras r WHERE r.fecha = :fecha")})
public class Registrodepalabras implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "palabra")
    private String palabra;
    @Column(name = "definicion")
    private Serializable definicion;
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private String fecha;

    public Registrodepalabras() {
    }

    public Registrodepalabras(String palabra) {
        this.palabra = palabra;
    }

    public String getPalabra() {
        return palabra;
    }

    public void setPalabra(String palabra) {
        this.palabra = palabra;
    }

    public Serializable getDefinicion() {
        return definicion;
    }

    public void setDefinicion(Serializable definicion) {
        this.definicion = definicion;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (palabra != null ? palabra.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Registrodepalabras)) {
            return false;
        }
        Registrodepalabras other = (Registrodepalabras) object;
        if ((this.palabra == null && other.palabra != null) || (this.palabra != null && !this.palabra.equals(other.palabra))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.diccionarioespanoloxford.entity.Registrodepalabras[ palabra=" + palabra + " ]";
    }
    
}
