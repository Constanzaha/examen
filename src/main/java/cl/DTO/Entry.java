/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.DTO;

import java.util.List;

/**
 *
 * @author const
 */
public class Entry{
    public List<String> etymologies;
    public List<GrammaticalFeature> grammaticalFeatures;
    public List<Sense> senses;
    
    public List<Sense> getSenses() {
        return this.senses;
    } 
    
    public void setSenses(List<Sense> senses) {
        this.senses = senses;
    } 
}
