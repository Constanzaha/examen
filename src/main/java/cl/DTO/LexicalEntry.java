/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.DTO;

import java.util.List;

/**
 *
 * @author const
 */
public class LexicalEntry{
    public List<Compound> compounds;
    public List<Derivate> derivatives;
    public List<Entry> entries;
    public String language;
    public LexicalCategory lexicalCategory;
    public String text;
    
    
    public List<Entry> getEntries() {
        return this.entries; 
    } 
    
    public void setEntries(List<Entry> entries) {
        this.entries = entries;
    } 
}